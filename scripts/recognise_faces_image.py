# USAGE
# python recognize_faces_image.py --encodings encodings.pickle --image examples/example_01.png 
import argparse
import pickle

import cv2
import face_recognition


# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	            help="path to input image")
ap.add_argument("-e", "--encodings", required=True,
	            help="path to serialized db of facial encodings")
ap.add_argument("-d", "--detection-method", type=str, default="cnn",
	            help="face detection model to use: either `hog` or `cnn`")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
	            help="Confidence value (0 -> 1): determine the face as recognised \
                if we have a percentage of matches greater than this value.")
args = vars(ap.parse_args())

# Load the known faces and their encodings
print("[INFO] loading encodings...")
data = pickle.loads(open(args["encodings"], "rb").read())

# Create a mapping which tells us how many encodings we have for each person
# we will use this to remove bias in the results
no_of_images = {}
for name in data["names"]:
    if name in no_of_images:
        # We've already saved, don't waste time
        continue
    total = data["names"].count(name)
    no_of_images[name] = total

# Load the input image and convert it from BGR to RGB
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Detect the (x, y)-coordinates of the bounding boxes corresponding
# to each face in the input image, then compute the facial embeddings
# for each face
print("[INFO] recognizing faces...")
boxes = face_recognition.face_locations(rgb, model=args["detection_method"])
current_encodings = face_recognition.face_encodings(rgb, boxes)

# Loop over the facial embeddings
determined_names = []
for encoding in current_encodings:
    # attempt to match each face in the input image to our known encodings
    # This function returns a bun of True/False values for each encoding
    # Internally, it's just computing the Euclidean distance between the encoding
    # and each encoding in our dataset
    matches = face_recognition.compare_faces(data["encodings"], encoding)
    name = "Unknown"

    # Check to see if we have found a match, if not we'll just label it as unknown
    if True in matches:
        # Find the indexes of all matched faces
        matchedIdxs = [i for (i, b) in enumerate(matches) if b]

        # Loop over the matched indexes and maintain a count for each recognised face
        counts = {}
        for i in matchedIdxs:
            name = data["names"][i]
            counts[name] = counts.get(name, 0) + 1

        # Determine the recognised face with the largest number of votes
        # by weighing this against the number of faces in the dataset
        ratios = {}
        for name in counts:
            ratios[name] = counts[name]/float(no_of_images[name])

        name = max(ratios, key=ratios.get)

        # Check against confidence
        if not ratios[name] > args["confidence"]:
            # Set it back to unknown
            name = "Unknown"

    # update the list of names
    determined_names.append(name)

# Loop over each recognised face in the image and label it
for ((top, right, bottom, left), name) in zip(boxes, determined_names):
    # Draw the predicted face name on the image
    cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)

    # Just incase the face is near the top of the image
    y = top - 15 if top - 15 > 15 else top + 15 
    cv2.putText(image, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
		        0.75, (0, 255, 0), 2)

# Show the output image
cv2.imshow("Image", image)
cv2.waitKey(0)
