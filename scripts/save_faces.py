# This simple python script is designed to save an image of whoever is in the
# webcam's face.

import argparse
import os
import shutil
import sys
import time

import cv2
import imutils
from imutils.video import VideoStream


ap = argparse.ArgumentParser()
ap.add_argument("-c", "--cascade", required=True,
	help = "Path to Haar cascade file")
ap.add_argument("-o", "--output", required=True,
	help="Path to output directory")
args = vars(ap.parse_args())

CASCADE = args["cascade"]
OUTPUT = args["output"]


# Check the output directory
if os.path.exists(OUTPUT):
    print(f"[INFO] {OUTPUT} already exists. Do you wish to overwrite it?")
    response = ""
    while response not in ['Y', 'N', 'y', 'n']:
        response = input("Please enter [Y] or [N]: ")

    # If yes, remove then create a fresh directory
    if response in ['Y', 'y']:
        print(f"[INFO] Removing the contents of {OUTPUT}")
        shutil.rmtree(OUTPUT)
        os.mkdir(OUTPUT)
    else:
        print("[INFO] Aborting...")
        sys.exit()
else:
    print(f"[INFO] Creating new directory: {OUTPUT}")
    os.mkdir(OUTPUT)


# Load OpenCV's Haar cascade file for face detection
detector = cv2.CascadeClassifier(CASCADE)

# Initialize the video stream allowing the camera sensor to warm up
print("[INFO] starting video stream...")
vs = VideoStream(src=0).start()  # 0 Will be first registered camera
time.sleep(2.0)

# Loop over the stream frame by frame
total_faces = 0
while True:
    # Grab the frame and copy it just in case we want to write it to
    # disk.
    frame = vs.read()
    clone = frame.copy()

    # Resize the frame so that we can quickly apply face detection
    frame = imutils.resize(frame, width=400)

    # Detect faces in a grayscaled frame (more efficient)
    # NOTE: Could potentially use another face detection algorithm here...
    detections = detector.detectMultiScale(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY),
                                           scaleFactor=1.1,
                                           minNeighbors=5, minSize=(30, 30))

	# Loop over detections and draw them on the frame
    for (x, y, w, h) in detections:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    # Show the output frame
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    # If the `s` key was pressed, write the *original* frame to disk
    if key == ord("s"):
        p = os.path.join(OUTPUT,
                         "{}.png".format(str(total_faces).zfill(5)))

        print("[INFO] Saving face to {}".format(p))
        cv2.imwrite(p, clone)
        total_faces += 1

    # If the `q` key was pressed, break from the loop
    elif key == ord("q"):
        break


print("[INFO] {} face images stored".format(total_faces))

# Cleanup
print("[INFO] cleaning up...")
cv2.destroyAllWindows()
vs.stop()
