# USAGE
# python encode_faces.py --dataset dataset --encodings encodings.pickle
import argparse
import os
import pickle

import cv2
import imutils
import face_recognition

# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--dataset", required=True,
	            help="path to input directory of faces + images")
ap.add_argument("-e", "--encodings", required=True,
	            help="path to serialized db of facial encodings")
# Before we can encode faces in images, we must detect them. We will use
# one of two different face detection methods: hog or cnn
ap.add_argument("-d", "--detection-method", type=str, default="cnn",
	            help="face detection model to use: either `hog` or `cnn`")
args = vars(ap.parse_args())

# Obtain image paths from the dataset
print("[INFO] quantifying faces...")
imagePaths = []
for (dirpath, dirnames, filenames) in os.walk(args["dataset"]):
    for f in filenames:
        imagePaths.append(os.path.join(dirpath, f))


# Initialise the lists of known encodings and known names
knownEncodings = []
knownNames = []
for (i, imagePath) in enumerate(imagePaths):
    # extract the person name from the image path
    name = imagePath.split(os.path.sep)[-2]

    # load the input image and convert it from BGR2RGB because dlib needs RGB
    image = cv2.imread(imagePath)
    h, w, _ = image.shape
    if h > 1000:
        print("Resizing image")
        image = imutils.resize(image, height=1000)
    elif w > 1000:
        print("Resizing image")
        image = imutils.resize(image, width=1000)

    rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    print("[INFO] processing image {}/{}. size: {}, shape: {}".format(i + 1,
                                                                      len(imagePaths),
                                                                      image.size,
                                                                      image.shape))

    # detect the (x, y)-coordinates of the bounding boxes
    # corresponding to each face in the input image
    boxes = face_recognition.face_locations(rgb,
		                                    model=args["detection_method"])

    # Compute the facial embedding for the faces
    encodings = face_recognition.face_encodings(rgb, boxes)

    # loop over the encodings
    for encoding in encodings:
        # add each encoding + name to the known lists
        knownEncodings.append(encoding)
        knownNames.append(name)

# Pickle the facial encodings + names to the specified file
print("[INFO] serializing encodings...")
data = {"encodings": knownEncodings, "names": knownNames}
f = open(args["encodings"], "wb")
f.write(pickle.dumps(data))
f.close()
