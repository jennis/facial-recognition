# Facial recognition with Python

This is a repo that contains all of the code related to _recognising_ faces
using OpenCV and Python. Note that I mean _recognising_, not _detecting_.

Detection can be thought of as a pre-requisite and I've done a bit on that
but it's in my general gitlab.com/jennis/computer-vision repo.


## Saving faces from the webcam
Perhaps you need to first build up a database with pictures of somebody's face.

The script [save_faces.py](scripts/save_faces.py) can be used to save faces
detected by your webcam.

To use:

```
python3 save_faces.py --cascade $path_to_cascade_file --output $path_to_output_dir
```

This script uses OpenCV's Haarcascade to detect faces. To save an image, press
"s" when the video is running, this will save it to `$path_to_output_dir`.

A cascade file has also been uploaded to this repo.

Press `q` to exit.


## Encoding faces
To start, we need to gather images of the faces we want to be able to recognise.
The more the merrier, and try to ensure that each image only contains one face,
and that that face is that face of the person that you wish to recognise.

Then try to save these images in adjacent folders, e.g.:
```
dataset/
  bob/
  john/
  carol/
```

Where each subdir in the dataset contains _only_ images of Bob, John and Carol's
faces, respectively.

Then, we need to _encode_ each and every one of these faces and save the
encodings to a file. We do this using [encode_faces.py](scripts/encode_faces.py):

```
python encode_faces.py --dataset $path_to_dataset --encodings $path_to_output_encodings
```

By default this script will use the more expensive cnn face detection algorithm,
to use a more lightweight but perhaps less accurate face detection algorithm
known as hog, you can add `--detection-method hog`


## Recognising the faces in an image
Once you have the appropriate encodings, we can try recognise the faces within an image.

To do that, use the [recognise_faces_image.py](scripts/recognise_faces_image.py)
script by passing in and image and using the encodings pickle file, like so:

```
python recognise_faces_image.py --image $path_to_image --encodings $path_to_encodings
```

This will read the image, detect each and every face and try to match them with
faces from your dataset. If a match can't be made, the face will be detected as
"unknown".

Note again, `--detection-method` can be changed to hog, default is cnn.

Additionally, a `--confidence` value can be set (default 0.2).


## Recognising the face in the webcam
To do this, we can use the
[recognise_faces_camera.py](scripts/recognise_faces_camera.py) script which will
load our webcam, analyse each frame and detect the faces present and try to find
a match. For each frame, we then draw on the rectangle and write the name of the
match and then show the frame, essentially creating another video stream.

To use:

```
python recognise_faces_camera.py --encodings $path_to_encodings
```

As usual, the `--detection-method` can also be selected as hog or cnn.

Additionally an `--output` flag can be used to save the output video. And the
`--display` flag can be set to 0 or 1 depending on whether or not you want to
display the video.


## Recognising faces in a video
Pretty much a hybrid between the previous two scripts,
[recognise_faces_video_file.py](scripts/recognise_faces_video_file.py) will
read a video file, analyse every frame and try to detect the faces present.

NOTE: This is computationally expensive, I would advise using the hog algorithm
here.

To use:

```
python recognise_faces_video_file.py --input $path_to_video --encodings $path_to_encodings
```

Again, you can select cnn or hog with the`--detection-method` flag.
We can choose to display the output to the screen with `--display 1` and we can save the output to a file with the `--output` flag.
